import numpy as np
import parameters as par
import lab1_utils as lu


# kp wpływa na siłę przyciągania przez punkt końcowy.
# Im większe kp tym większe wartości związane z odległością od punktu końcowego.
#
# koi wpływa na siłe odpychania przez przeszkody.
# Im większe koi tym mocniej przeszkody "odpychają od siebie".
#
# d0 wpływa na siłe odpychania przez przeszkody.
# Im większy parametr d0 tym większy jest promień odpychania przez przeszkody oraz wartość odpychania zmienia się mniej gwałtownie niż w przypadku małego parametru d0.

kp, koi, d0 = par.startowe_parametry()
start_point = (-10, lu.rand_number())
finish_point = (10, lu.rand_number())
robot = start_point
delta = 0.1

obstacles = [(lu.rand_number(), lu.rand_number()), (lu.rand_number(), lu.rand_number()), (lu.rand_number(), lu.rand_number()), (lu.rand_number(), lu.rand_number())]


@np.vectorize
def calcZ(qrx, qry):
	qr = [qrx, qry]
	Zmatrix = [0, 0, 0, 0, 0]
	Zmatrix[0] = lu.Fp(qr, finish_point, kp)
	Zmatrix[1] = lu.Foi(qr, obstacles[0], koi, d0)
	Zmatrix[2] = lu.Foi(qr, obstacles[1], koi, d0)
	Zmatrix[3] = lu.Foi(qr, obstacles[2], koi, d0)
	Zmatrix[4] = lu.Foi(qr, obstacles[3], koi, d0)
	return Zmatrix[0]-Zmatrix[1]-Zmatrix[2]-Zmatrix[3]-Zmatrix[4]


x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = calcZ(X, Y)

lu.plot(start_point, finish_point, robot, obstacles, Z)
