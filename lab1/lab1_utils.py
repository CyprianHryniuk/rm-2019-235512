import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt

def rand_number():
	return np.random.randint(21, size = 1) - 10


def Up(qr, qk, kp):
    sum_vec = [0, 0]
    sum_vec[0] = qr[0] - qk[0]
    sum_vec[1] = qr[1] - qk[1]
    return 0.5 * kp * np.linalg.norm(sum_vec) * np.linalg.norm(sum_vec)


def Voi(qr, qoi, koi, d0):
    sum_vec = [0, 0]
    sum_vec[0] = qr[0] - qoi[0]
    sum_vec[1] = qr[1] - qoi[1]
    if np.linalg.norm(sum_vec) <= d0:
        return 0.5 * koi * (1 / np.linalg.norm(sum_vec) - 1 / d0) * (1 / np.linalg.norm(sum_vec) - 1 / d0)
    else:
        return 0


def Uw(qr, qk, obstacles, kp, koi, d0):
    Vsum = 0
    for obstacle in obstacles:
        Vsum += Voi(qr, obstacle, koi, d0)
    return Up(qr, qk, kp) + Vsum


def Fp(qr, qk, kp):
    sum_vec = [0, 0]
    sum_vec[0] = qr[0] - qk[0]
    sum_vec[1] = qr[1] - qk[1]
    return kp * np.linalg.norm(sum_vec)


def Foi(qr, qoi, koi, d0):
    sum_vec = [0, 0]
    sum_vec[0] = qr[0] - qoi[0]
    sum_vec[1] = qr[1] - qoi[1]
    if np.linalg.norm(sum_vec) <= d0:
        return -1 * koi * (1 / np.linalg.norm(sum_vec) - 1 / d0) * (1 / (np.linalg.norm(sum_vec) * np.linalg.norm(sum_vec)))
    else:
        return 0


def plot(start_point, finish_point, robot, obstacles, Z):
    x_size = 10
    y_size = 10
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111)
    ax.set_title('Metoda potencjałów')
    plt.imshow(Z, cmap=cm.RdYlGn,
               origin='lower', extent=[-x_size, x_size, -y_size, y_size],
               vmax=60, vmin=0)

    plt.plot(start_point[0], start_point[1], "or", color='blue')
    plt.plot(finish_point[0], finish_point[1], "or", color='blue')
    plt.plot(robot[0], robot[1], "or", color='red')

    for obstacle in obstacles:
        plt.plot(obstacle[0], obstacle[1], "or", color='black')

    plt.colorbar(orientation='vertical')

    plt.grid(True)
    plt.show()
