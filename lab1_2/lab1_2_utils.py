import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from math import atan2, cos, sin


def rand_number():
    return (np.random.randint(21, size=1) - 10)[0]


def Up(qr, qk, kp):
    sum_vec = [0, 0]
    sum_vec[0] = qr[0] - qk[0]
    sum_vec[1] = qr[1] - qk[1]
    return 0.5 * kp * np.linalg.norm(sum_vec) * np.linalg.norm(sum_vec)


def Voi(qr, qoi, koi, d0):
    if qr[0] == qoi[0] and qr[1] == qoi[1]:
        return 0
    sum_vec = [0, 0]
    sum_vec[0] = qr[0] - qoi[0]
    sum_vec[1] = qr[1] - qoi[1]
    if np.linalg.norm(sum_vec) <= d0:
        return 0.5 * koi * (1 / np.linalg.norm(sum_vec) - 1 / d0) * (1 / np.linalg.norm(sum_vec) - 1 / d0)
    else:
        return 0


def Uw(qr, qk, obstacles, kp, koi, d0):
    Vsum = 0
    for obstacle in obstacles:
        Vsum += Voi(qr, obstacle, koi, d0)
    return Up(qr, qk, kp) + Vsum


def Fp(qr, qk, kp):
    sum_vec = [0, 0]
    sum_vec[0] = qr[0] - qk[0]
    sum_vec[1] = qr[1] - qk[1]
    return kp * np.linalg.norm(sum_vec)


def Foi(qr, qoi, koi, d0):
    if qr == qoi:
        return 0
    sum_vec = [0, 0]
    sum_vec[0] = qr[0] - qoi[0]
    sum_vec[1] = qr[1] - qoi[1]
    if np.linalg.norm(sum_vec) <= d0:
        return -1 * koi * (1 / np.linalg.norm(sum_vec) - 1 / d0) * (1 / (np.linalg.norm(sum_vec) * np.linalg.norm(sum_vec)))
    else:
        return 0


def plot(start_point, finish_point, robot, obstacles, Z, robot_path):
    x_size = 10
    y_size = 10

    plt.imshow(Z, cmap=cm.RdYlGn,
               origin='lower', extent=[-x_size, x_size, -y_size, y_size],
               vmax=9, vmin=0)

    plt.plot(start_point[0], start_point[1], "or", color='blue')
    plt.plot(finish_point[0], finish_point[1], "or", color='blue')
    plt.plot(robot[0], robot[1], "or", color='red')
    for point in robot_path:
        plt.plot(point[0], point[1], "or", color='blue')

    for obstacle in obstacles:
        plt.plot(obstacle[0], obstacle[1], "or", color='black')

    plt.colorbar(orientation='vertical')
    plt.grid(True)


def calc_angle(pointA, pointB):
    vec = [pointB[0] - pointA[0], pointB[1] - pointA[1]]
    return atan2(vec[1], vec[0])


def calc_xy(vector):
    x = vector[1] * cos(vector[0])
    y = vector[1] * sin(vector[0])
    return [x, y]


def calc_out_angle(finish_vec, obstacle_vec):
    finish_vec = calc_xy(finish_vec)
    x = finish_vec[0]
    y = finish_vec[1]
    for obstacle in obstacle_vec:
        obstacle = calc_xy(obstacle)
        x = x + obstacle[0]
        y = y + obstacle[1]
    return calc_angle([0, 0], [x, y])


def move_robot(robot, next_point):
    if next_point == 1:
        return [robot[0]+0.1, robot[1]]
    if next_point == 2:
        return [robot[0]+0.1, robot[1]+0.1]
    if next_point == 3:
        return [robot[0], robot[1]+0.1]
    if next_point == 4:
        return [robot[0]-0.1, robot[1]+0.1]
    if next_point == 5:
        return [robot[0]-0.1, robot[1]]
    if next_point == 6:
        return [robot[0]-0.1, robot[1]-0.1]
    if next_point == 7:
        return [robot[0], robot[1]-0.1]
    if next_point == 8:
        return [robot[0]+0.1, robot[1]-0.1]

