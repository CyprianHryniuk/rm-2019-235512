import numpy as np
import parameters as par
import lab1_2_utils as lu
from math import sqrt
import matplotlib.pyplot as plt
from drawnow import drawnow

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')

kp, koi, d0, dr = par.startowe_parametry()
start_point = (-10, lu.rand_number())
finish_point = (10, lu.rand_number())
robot = start_point
delta = 0.1
obstacles = [(lu.rand_number(), lu.rand_number()), (lu.rand_number(), lu.rand_number()), (lu.rand_number(), lu.rand_number()), (lu.rand_number(), lu.rand_number())]

@np.vectorize
def calcZ(qrx, qry):
	qr = [qrx, qry]
	Zmatrix = [0, 0, 0, 0, 0]
	Zmatrix[0] = lu.Fp(qr, finish_point, kp)
	Zmatrix[1] = lu.Foi(qr, obstacles[0], koi, d0)
	Zmatrix[2] = lu.Foi(qr, obstacles[1], koi, d0)
	Zmatrix[3] = lu.Foi(qr, obstacles[2], koi, d0)
	Zmatrix[4] = lu.Foi(qr, obstacles[3], koi, d0)
	return Zmatrix[0]-Zmatrix[1]-Zmatrix[2]-Zmatrix[3]-Zmatrix[4]


x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)


@np.vectorize
def find_next_point(qrx, qry, dr):
	qr = [qrx, qry]
	finish_vec = [lu.calc_angle(qr, finish_point), lu.Fp(qr, finish_point, kp)]
	obstacle_vec = []
	for obstacle in obstacles:
		if sqrt((robot[0]-obstacle[0])**2 + (robot[1]-obstacle[1])**2) < dr:
			obstacle_vec.append([lu.calc_angle(qr, obstacle), lu.Foi(qr, obstacle, koi, d0)])
	out_angle = lu.calc_out_angle(finish_vec, obstacle_vec)
	out_angle = out_angle + np.pi
	if out_angle <= np.pi/8:
		return 5.0
	elif out_angle <= 3*np.pi/8:
		return 6.0
	elif out_angle <= 5*np.pi/8:
		return 7.0
	elif out_angle <= 7*np.pi/8:
		return 8.0
	elif out_angle <= 9*np.pi/8:
		return 1.0
	elif out_angle <= 11*np.pi/8:
		return 2.0
	elif out_angle <= 13*np.pi/8:
		return 3.0
	elif out_angle <= 15*np.pi/8:
		return 4.0
	else:
		return 5.0


robot_path = []
Z = find_next_point(X, Y, 999)


def update():
	lu.plot(start_point, finish_point, robot, obstacles, Z, robot_path)


while robot[0] != finish_point[0] or robot[1] != finish_point[1]:
	robot_path.append(robot)
	robot = lu.move_robot(robot, find_next_point(robot[0], robot[1], dr))
	drawnow(update)
	if abs(robot[0] - finish_point[0]) < 0.2 and abs(robot[1]-finish_point[1]) < 0.2:
		robot = finish_point
		robot_path.append(robot)
		break

drawnow(update)
plt.show()

